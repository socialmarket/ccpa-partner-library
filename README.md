# CCPA-Jurisdiction-PHP-SDK-Partners
CCPA-Jurisdiction-PHP-SDK for Third Partners  
Version: 20200226

## Installation

For example, you can extract SDK files to your htdocs folder (www root).

```bash
tar -zxvf jurisdiction_partners.tar.gz
```

## Usage

Once you have the SDK installed, you can use it like in the following example:

```php
<?php 
require_once 'ccpa_autoload.php';
use Oath\Corp\Tools\CCPAJurisdiction\CCPAJurisdiction;

$namespace = CCPAJurisdiction::NAMESPACE_YAHOO;
$locale = 'zh-Hant-TW';
$headers = getallheaders();

$ccpa = new CCPAJurisdiction("config.json");
$data = $ccpa->getData($namespace, $locale, $headers);

if(!empty($data['label'])) {
    echo '<a href="' . $data['url'] . '">' . $data['label'];
    if (isset($data['showDAAPrivacyRightsIcon']) && $data['showDAAPrivacyRightsIcon']) {
        echo '<img src="https://s.yimg.com/cv/apiv2/default/20200109/Privacy_Rights_icon.png" width="15" style="vertical-align: middle;padding-left: 1px;">';
    }
    echo '</a>';
}
```

### Input Parameters

The `getData()` method supports the following input parameters.  All are optional.

| Parameter | Description |
| --------- | ----------- |
| `$namespace` | Namespace corresponds to the property.  See the [Jurisdiction Config](https://git.ouroath.com/CorpApps-Engineering/CCPA-Jurisdiction-Config/blob/master/config.json) for all supported namespaces.  The `CCPAJurisdiction` class also has `NAMESPACE_*` constants for each namespace.  If this parameter not specified. a default namespace will be used. |
| `$languageCode` | A [ISO 639-1 language code](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes).  This determines the language of the data returned.  If not specified, a default value is used. |
| `$headers` | An associative array of headers with keys being the header name and values being the value of the header.  If not specified, `getallheaders()` is used. |

### Output

The `getData()` method returns an associative array with two elements:

| Array Key | Description |
| --------- | ----------- |
| `label` | The text to display for the link |
| `url` | The URL target for the link |
