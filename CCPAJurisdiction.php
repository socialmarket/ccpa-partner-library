<?php 

namespace Oath\Corp\Tools\CCPAJurisdiction;

use Oath\Corp\Tools\CCPAJurisdictionConfig\Loader;


class CCPAJurisdiction
{
    const JURISDICTION_HEADER_NAME = 'x-jurisdiction-type';
    const JURISDICTION_HEADER_NAME_CAMEL = 'X-Jurisdiction-Type';

    const JURISDICTION_GDPR  = 'GDPR';
    const JURISDICTION_CCPA  = 'CCPA';
    const JURISDICTION_US    = 'US';
    const JURISDICTION_WORLD = 'WORLD';

    const NAMESPACE_AOL                 = "aol";
    const NAMESPACE_AUTOBLOG            = "autoblog";
    const NAMESPACE_BUILDSERIES         = "buildseries";
    const NAMESPACE_BUILT_BY_GIRLS      = "builtbygirls";
    const NAMESPACE_ENGADGET            = "engadget";
    const NAMESPACE_HUFFPOST            = "huffpost";
    const NAMESPACE_MAKERS              = "makers";
    const NAMESPACE_RIVALS              = "rivals";
    const NAMESPACE_RYOT                = "ryot";
    const NAMESPACE_TECHCRUNCH          = "techcrunch";
    const NAMESPACE_YAHOO               = "yahoo";

    const BRAND_ATT                     = "att";
    const BRAND_BT                      = "bt";
    const BRAND_SKY                     = "sky";
    const BRAND_FRONTIER                = "frontier";
    const BRAND_ROGERS                  = "rogers";
    

    private $configFile;


    public function __construct($configFile = null)
    {
        $this->configFile = $configFile;
    }


    /**
     * Returns the URL and label to use for CCPA
     *
     * @param string $namespace
     * @param string $locale
     * @param array $headers
     * @return array Array with keys "url" and "label"
     * @throws \Exception
     */
    public function getData($namespace = '', $locale = '', array $headers = array())
    {
        $config = $this->loadConfigFile();
        $labelLocale = $locale;
        $urlLocale = $locale;
        $warning = array();

        $urlKey = $namespace;

        if (empty($headers) && function_exists('getallheaders')) {
            $headers = getallheaders();
        }

        if (empty($urlKey) || !isset($config['urls'][$urlKey])) {
            $urlKey = 'default';
            $warning[] = 'Invalid namespace or brand, using default';
        }

        if (!isset($config['urls'][$urlKey][$urlLocale])) {
            $urlLocale = 'default';
        }

        if (!isset($config['labels'][$labelLocale])) {
            $labelLocale = 'default';
            $warning[] = 'Invalid locale, using default';
        }

        $isCCPA = $this->isCCPA($headers);
        $jurisdictionKey = $isCCPA ? 'ccpa' : 'default';

        if ($this->isOutsideUS($headers)) {
            return array(
                'url' => '',
                'label' => '',
                'warning' => 'Jurisdiction type not applicable',
                'showDAAPrivacyRightsIcon' => $isCCPA
            );
        }

        return array(
            'url' => $this->parseUrlTags($config['urls'][$urlKey][$urlLocale][$jurisdictionKey], $locale ?: 'zh-Hant-TW'),
            'label' => $config['labels'][$labelLocale][$jurisdictionKey],
            'warning' => implode('; ', $warning),
            'showDAAPrivacyRightsIcon' => $isCCPA
        );
    }


    private function loadConfigFile()
    {
        if ($this->configFile) {
            if (is_readable($this->configFile)) {
                $jsonStr = file_get_contents($this->configFile);
                return json_decode($jsonStr, true);
            } else {
                throw new \Exception('Unable to read config file [' . $this->configFile . ']');
            }
        } else {
            return Loader::load();
        }
    }


    private function isCCPA(array $headers)
    {
        $data = $this->getJurisdictionHeaderData($headers);
        if ($data === null) {
            return false;
        }

        return ($data == self::JURISDICTION_CCPA);
    }


    private function isOutsideUS(array $headers)
    {
        $data = $this->getJurisdictionHeaderData($headers);
        if ($data === null) {
            return false;
        }

        return !(in_array($data, array(self::JURISDICTION_CCPA, self::JURISDICTION_US)));
    }

    private function getJurisdictionHeaderData(array $headers)
    {
        if (array_key_exists(self::JURISDICTION_HEADER_NAME, $headers)) {
            return $headers[self::JURISDICTION_HEADER_NAME];
        }

        if (array_key_exists(self::JURISDICTION_HEADER_NAME_CAMEL, $headers)) {
            return $headers[self::JURISDICTION_HEADER_NAME_CAMEL];
        }

        return null;
    }

    private function parseUrlTags($url, $locale) {
        return str_replace('{locale}', $locale, $url);
    }
}

